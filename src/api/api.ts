import axios from 'axios'

const BASE_URL = 'https://api.spacexdata.com'

export interface ApiLaunchesData {
  id: string
  flight_number: number
  name: string
  date_utc: string
  success: boolean
  details: string | null
  rocket: string
  links: {
    presskit: string | null
    article: string | null
    wikipedia: string | null
  }
}

export const getLaunches = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/v5/launches`)
    return { data: response.data as ApiLaunchesData[], error: null }
  } catch (error) {
    return { data: null, error: error }
  }
}

export const getLaunch = async (id: string) => {
  try {
    const response = await axios.get(`${BASE_URL}/v5/launches/${id}`)
    return { data: response.data as ApiLaunchesData, error: null }
  } catch (error) {
    return { data: null, error: error }
  }
}
