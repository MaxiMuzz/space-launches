import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { AppDispatch, RootState } from '../../store'
import { LaunchesActionTypes } from '../../store/launchesReducer/launchesActions'
import { DraggableItemTypes } from '../Draggable/constants'
import { LaunchesList } from '../LaunchesList'

export const BookedLaunchesList: React.FC = () => {
  const bookedLaunchesData = useSelector(({ launches }: RootState) =>
    launches.bookedLaunches.sort((a, b) => a.date.getTime() - b.date.getTime())
  )
  const dispatch = useDispatch<AppDispatch>()

  const handleDrop = useCallback(
    (item: { id: string }) => {
      /*
        Complete a launch booking request, then run dispatch
      */

      dispatch({
        type: LaunchesActionTypes.BOOK_LAUNCH,
        payload: { id: item.id },
      })
    },
    [dispatch]
  )

  const canDrop = useCallback(
    (item: { id: string }) => {
      const droppingLaunch = bookedLaunchesData.find(({ id }) => id === item.id)

      return !droppingLaunch
    },
    [bookedLaunchesData]
  )

  return (
    <LaunchesList
      title="My launches"
      data={bookedLaunchesData}
      droppableOptions={{
        accept: DraggableItemTypes.LAUNCH,
        canDrop,
        drop: handleDrop,
      }}
    />
  )
}
