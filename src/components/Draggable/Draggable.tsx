import { useDrag } from 'react-dnd'
import styled from 'styled-components'

export interface iDraggableProps {
  type: string | symbol
  item: object | Function
  disabled?: boolean
  children?: React.ReactNode
}

export const Draggable: React.FC<iDraggableProps> = ({
  type,
  item,
  disabled = false,
  children,
}) => {
  const [{ isDragging }, drag] = useDrag(
    () => ({
      type,
      item,
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
    }),
    [type, item]
  )

  return !disabled ? (
    <Wrapper isDragging={isDragging} ref={drag}>
      {children}
    </Wrapper>
  ) : (
    <div>{children}</div>
  )
}

const Wrapper = styled.div<{ isDragging: boolean }>`
  opacity ${({ isDragging }) => (isDragging ? '0.7' : '1')}
`
