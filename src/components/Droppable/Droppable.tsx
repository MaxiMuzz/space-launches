import { DropTargetMonitor, useDrop } from 'react-dnd'
import styled from 'styled-components'

export interface iDroppableProps {
  accept: string | symbol | string[] | symbol[]
  drop?: (item: any, monitor: DropTargetMonitor) => void
  canDrop?: (item: any, monitor: DropTargetMonitor<unknown, void>) => boolean
  children?: React.ReactNode
  className?: string
  disabled?: boolean
}

export const Droppable: React.FC<iDroppableProps> = ({
  className,
  accept,
  drop,
  canDrop,
  children,
  disabled,
}) => {
  const [{ _canDrop, _isOver }, dropRef] = useDrop(
    () => ({
      accept,
      drop,
      canDrop,
      collect: (monitor) => ({
        _canDrop: monitor.canDrop(),
        _isOver: monitor.isOver(),
      }),
    }),
    [accept, drop, canDrop]
  )

  return !disabled ? (
    <Wrapper
      className={className}
      ref={dropRef}
      canDrop={_canDrop}
      isOver={_isOver}
    >
      {children}
    </Wrapper>
  ) : (
    <div>{children}</div>
  )
}

const Wrapper = styled.div<{ canDrop: boolean; isOver: boolean }>`
  position: relative;
  background-color: ${({ isOver, canDrop }) =>
    isOver && canDrop ? 'rgba(0, 0, 0, 0.02)' : 'rgba(0, 0, 0, 0)'};
  transition: background-color ease 0.05s;
`
