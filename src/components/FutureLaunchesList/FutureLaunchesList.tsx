import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Modal } from 'antd'

import { AppDispatch, RootState } from '../../store'
import { LaunchesActionTypes } from '../../store/launchesReducer/launchesActions'
import { DraggableItemTypes } from '../Draggable/constants'
import { LaunchesList } from '../LaunchesList'

const { confirm } = Modal

export const FutureLaunchesList: React.FC = () => {
  const launchesData = useSelector(({ launches }: RootState) =>
    launches.launches.sort((a, b) => a.date.getTime() - b.date.getTime())
  )
  const dispatch = useDispatch<AppDispatch>()

  const handleDrop = useCallback(
    (item: { id: string; name: string }) => {
      confirm({
        title: `Are you sure you want to cancel '${item.name}' launch booking?`,
        icon: <ExclamationCircleOutlined />,
        onOk() {
          /* 
            Complete a cancellation request, then run dispatch
          */

          dispatch({
            type: LaunchesActionTypes.CANCEL_LAUNCH_BOOKING,
            payload: { id: item.id },
          })
        },
      })
    },
    [dispatch]
  )

  const canDrop = useCallback(
    (item: { id: string }) => {
      const droppingLaunch = launchesData.find(({ id }) => id === item.id)

      return !droppingLaunch
    },
    [launchesData]
  )

  return (
    <LaunchesList
      title="Launches"
      data={launchesData}
      droppableOptions={{
        accept: DraggableItemTypes.LAUNCH,
        canDrop,
        drop: handleDrop,
      }}
    />
  )
}
