import { Card, Typography } from 'antd'
import Link from 'next/link'
import styled from 'styled-components'

import { Draggable, iDraggableProps } from '../Draggable'
import { DraggableItemTypes } from '../Draggable/constants'

export interface iLaunchData {
  id: string
  name: string
  date: Date
  flightNumber: number
  success: boolean
  details: string | null
  rocket: string
  links: {
    presskit: string | null
    article: string | null
    wikipedia: string | null
  }
  loading?: boolean
}

interface iProps {
  launchInfo: iLaunchData
  draggableOptions?: Partial<Omit<iDraggableProps, 'children'>>
}

const defaultDraggableOptions: Omit<iDraggableProps, 'children'> = {
  disabled: false,
  type: DraggableItemTypes.LAUNCH,
  item: {},
}

export const LaunchCard: React.FC<iProps> = ({
  launchInfo,
  draggableOptions = {},
}) => {
  const { name, date, success, id: launchId } = launchInfo
  return (
    <Draggable
      {...defaultDraggableOptions}
      {...draggableOptions}
      item={{ id: launchId, name }}
    >
      <Link href={`/launches/${launchId}`}>
        <a>
          <StyledCard
            bordered
            size="small"
            draggable={!draggableOptions.disabled}
          >
            <Typography.Paragraph>{name}</Typography.Paragraph>
            <Typography.Paragraph>
              {date.toLocaleDateString()}
            </Typography.Paragraph>
            {date.getTime() < Date.now() && (
              <Typography.Paragraph
                style={{ color: success ? 'green' : 'red' }}
              >
                {success ? 'Success' : 'Failure'}
              </Typography.Paragraph>
            )}
          </StyledCard>
        </a>
      </Link>
    </Draggable>
  )
}

const StyledCard = styled(Card)<{ draggable: boolean }>`
  cursor: ${({ draggable }) => (draggable ? 'grab' : 'pointer')};

  div {
    margin-bottom: 5px;

    &:last-child {
      margin: 0;
    }
  }
`
