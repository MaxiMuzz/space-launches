import { List, Skeleton, Typography } from 'antd'
import styled from 'styled-components'

import { iDraggableProps } from '../Draggable'
import { Droppable, iDroppableProps } from '../Droppable'
import { iLaunchData, LaunchCard } from '../LaunchCard'

interface iProps {
  data: iLaunchData[]
  droppableOptions: Omit<iDroppableProps, 'children' | 'className'>
  draggableOptions?: Partial<Omit<iDraggableProps, 'children'>>
  title?: string
}

export const LaunchesList: React.FC<iProps> = ({
  title,
  data,
  draggableOptions = { disabled: false },
  droppableOptions,
}) => {
  return (
    <Wrapper>
      {title && <ColumnTitle>{title}</ColumnTitle>}
      <Droppable {...droppableOptions}>
        <ListWrapper>
          <List
            itemLayout="vertical"
            dataSource={data}
            renderItem={(item) => (
              <ListItem>
                <StyledSkeleton
                  loading={item.loading}
                  active
                  title={false}
                  paragraph={{ rows: 3, width: ['50%', '90%', '70%'] }}
                >
                  <LaunchCard
                    draggableOptions={draggableOptions}
                    launchInfo={item}
                  />
                </StyledSkeleton>
              </ListItem>
            )}
          />
        </ListWrapper>
      </Droppable>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  & > div:last-child {
    height: 100%;
    max-height: calc(100% - 36px);
  }
`

const ColumnTitle = styled(Typography.Paragraph)`
  text-transform: uppercase;
  text-align: center;
`

const ListWrapper = styled.div`
  padding: 10px;
  border: 1px solid rgba(210, 210, 210, 1);
  border-radius: 4px;
  height: 100%;
  max-height: 100%;
  overflow-y: auto;

  & li {
    padding: 0;
  }
`

const ListItem = styled(List.Item)`
  margin: 0 0 10px;

  &:last-child {
    margin: 0;
  }
`

const StyledSkeleton = styled(Skeleton)`
  padding: 12px;
  background-color: white;
  cursor: default;
`
