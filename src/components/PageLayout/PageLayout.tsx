import { LoadingOutlined } from '@ant-design/icons'
import { Layout, Typography } from 'antd'
import Link from 'next/link'
import styled from 'styled-components'

const { Header, Content } = Layout

interface iProps {
  children: React.ReactNode
  isLoading?: boolean
}

export const PageLayout: React.FC<iProps> = ({
  children,
  isLoading = false,
}) => {
  return (
    <StyledPageLayout>
      <Header>
        <Container>
          <Link href="/">
            <a>
              <HeaderTitle>Space Launches</HeaderTitle>
            </a>
          </Link>
        </Container>
      </Header>
      <Content>
        <Container>
          <MainContent>
            {isLoading ? (
              <LoadingWrapper>
                <StyledLoadingOutlined />
              </LoadingWrapper>
            ) : (
              children
            )}
          </MainContent>
        </Container>
      </Content>
    </StyledPageLayout>
  )
}

const StyledPageLayout = styled(Layout)`
  min-height: 100vh;
  overflow: hidden;
`

export const Container = styled.div`
  width: 100%;
  height: 100%;
  max-width: 1180px;
  padding: 0 15px;
  margin: 0 auto;

  @media (max-width: 1180px) {
    max-width: 992px;
  }
  @media (max-width: 992px) {
    max-width: 768px;
  }
  @media (max-width: 768px) {
    max-width: 600px;
  }
`

const HeaderTitle = styled(Typography.Text)`
  color: white;
  font-size: 26px;
  text-decoration: underline;
`

export const MainTitle = styled(Typography.Title)`
  text-align: center;
`

const MainContent = styled.div`
  padding: 20px 0 20px 0;

  & ${MainTitle} {
    font-size: 32px;
    font-weight: 400;
    margin-bottom: 20px;
  }
`

const LoadingWrapper = styled.div`
  margin-top: 100px;
  display: flex;
  justify-content: center;
`

const StyledLoadingOutlined = styled(LoadingOutlined)`
  svg {
    width: 40px;
    height: 40px;
  }
`
