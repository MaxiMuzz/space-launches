import { useSelector } from 'react-redux'

import { RootState } from '../../store'
import { LaunchesList } from '../LaunchesList'

export const PastLaunchesList: React.FC = () => {
  const pastLaunchesData = useSelector(
    ({ launches }: RootState) => launches.pastLaunches
  )

  return (
    <LaunchesList
      title="Past Launches"
      data={pastLaunchesData}
      droppableOptions={{
        disabled: true,
        accept: '',
      }}
      draggableOptions={{
        disabled: true,
      }}
    />
  )
}
