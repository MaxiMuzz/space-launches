import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'
import Head from 'next/head'

import store from '../store'

import '../styles/globals.scss'
import 'antd/dist/antd.css'

function SpaceLaunchesApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Head>
        <title>Space Launches</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Component {...pageProps} />
    </Provider>
  )
}

export default SpaceLaunchesApp
