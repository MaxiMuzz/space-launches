import { useEffect } from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { useDispatch, useSelector } from 'react-redux'
import { Col, Row } from 'antd'
import type { NextPage } from 'next'
import styled from 'styled-components'

import api from '../api'
import { BookedLaunchesList } from '../components/BookedLaunchesList'
import { FutureLaunchesList } from '../components/FutureLaunchesList'
import { MainTitle, PageLayout } from '../components/PageLayout'
import { PastLaunchesList } from '../components/PastLaunchesList'
import { AppDispatch, RootState } from '../store'
import { LaunchesActionTypes } from '../store/launchesReducer/launchesActions'

const Home: NextPage = () => {
  const dispatch = useDispatch<AppDispatch>()
  const bookedLaunches = useSelector(
    ({ launches }: RootState) => launches.bookedLaunches
  )

  useEffect(() => {
    api.getLaunches().then(({ data }) => {
      if (data) {
        const mappedData = data.map(({ date_utc, flight_number, ...rest }) => ({
          ...rest,
          flightNumber: flight_number,
          date: new Date(date_utc),
        }))

        dispatch({
          type: LaunchesActionTypes.SET_PAST_LAUNCHES,
          payload: {
            data: mappedData.filter(({ date }) => date.getTime() < Date.now()),
          },
        })
        dispatch({
          type: LaunchesActionTypes.SET_LAUNCHES,
          payload: {
            data: mappedData.filter(
              ({ id, date }) =>
                date.getTime() > Date.now() &&
                !bookedLaunches.find((bookedLaunch) => bookedLaunch.id === id)
            ),
          },
        })
      }
    })
  }, [dispatch])

  return (
    <DndProvider backend={HTML5Backend}>
      <PageLayout>
        <MainTitle level={1}>Explore the Space</MainTitle>
        <StyledRow gutter={10}>
          <StyledCol span={8}>
            <PastLaunchesList />
          </StyledCol>
          <StyledCol span={8}>
            <FutureLaunchesList />
          </StyledCol>
          <StyledCol span={8}>
            <BookedLaunchesList />
          </StyledCol>
        </StyledRow>
      </PageLayout>
    </DndProvider>
  )
}

export default Home

const StyledRow = styled(Row)`
  height: calc(100vh - 164px);
`

const StyledCol = styled(Col)`
  max-height: 100%;
`
