import { useEffect, useState } from 'react'
import { ArrowLeftOutlined } from '@ant-design/icons'
import { Typography } from 'antd'
import { NextPage } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styled from 'styled-components'

import api from '../../api'
import { iLaunchData } from '../../components/LaunchCard'
import { MainTitle, PageLayout } from '../../components/PageLayout'

const LaunchPage: NextPage = () => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(true)
  const [launchInfo, setLaunchInfo] = useState<iLaunchData | null>(null)

  useEffect(() => {
    router.query.id &&
      api.getLaunch(router.query.id as string).then(({ data }) => {
        data &&
          setLaunchInfo({
            ...data,
            date: new Date(data.date_utc),
            flightNumber: data.flight_number,
          })

        setIsLoading(false)
      })
  }, [router.query.id])

  return (
    <PageLayout isLoading={isLoading}>
      <Wrapper>
        <TitleRow>
          <Link href="/" passHref>
            <GoBackLink>
              <ArrowLeftOutlined /> Back
            </GoBackLink>
          </Link>
          <MainTitle>{launchInfo?.name}</MainTitle>
        </TitleRow>
        <ContentWrapper>
          {launchInfo?.details && (
            <Typography.Paragraph>{launchInfo?.details}</Typography.Paragraph>
          )}
          {launchInfo?.rocket && (
            <Typography.Paragraph>
              <Bold>Rocket:</Bold> {launchInfo.flightNumber}
            </Typography.Paragraph>
          )}
          {launchInfo?.date && (
            <Typography.Paragraph>
              <Bold>Launch date:</Bold> {launchInfo.date.toLocaleDateString()}
            </Typography.Paragraph>
          )}
          {launchInfo?.flightNumber && (
            <Typography.Paragraph>
              <Bold>Flight number:</Bold> {launchInfo.flightNumber}
            </Typography.Paragraph>
          )}
          {launchInfo?.date && launchInfo?.date.getTime() < Date.now() && (
            <Typography.Paragraph>
              <Bold>Launch result:</Bold>{' '}
              <LaunchResult isLaunchSucceed={launchInfo?.success || false}>
                {launchInfo?.success ? 'success' : 'failure'}
              </LaunchResult>
            </Typography.Paragraph>
          )}
          {(launchInfo?.links?.presskit ||
            launchInfo?.links?.article ||
            launchInfo?.links?.wikipedia) && (
            <Links>
              <Typography.Paragraph>
                <Bold>Links:</Bold>
              </Typography.Paragraph>
              {launchInfo?.links?.presskit && (
                <Typography.Paragraph>
                  <a href={launchInfo?.links?.presskit}>Presskit</a>
                </Typography.Paragraph>
              )}
              {launchInfo?.links?.article && (
                <Typography.Paragraph>
                  <a href={launchInfo?.links?.article}>Article</a>
                </Typography.Paragraph>
              )}
              {launchInfo?.links?.wikipedia && (
                <Typography.Paragraph>
                  <a href={launchInfo?.links?.wikipedia}>Wikipedia</a>
                </Typography.Paragraph>
              )}
            </Links>
          )}
        </ContentWrapper>
      </Wrapper>
    </PageLayout>
  )
}

export default LaunchPage

const Wrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`

const TitleRow = styled.div`
  position: relative;
  display: flex;
  align-items: start;
  justify-content: center;
  padding: 0 55px;
`

const GoBackLink = styled.a`
  position: absolute;
  left: 0;
  top: 10px;
  color: #001529;

  &:hover {
    color: #0254a2;
  }
`

const ContentWrapper = styled.div`
  background-color: white;
  padding: 20px;

  div:last-child {
    margin: 0;
  }
`

const Bold = styled.span`
  font-weight: 700;
`

const LaunchResult = styled.span<{ isLaunchSucceed: boolean }>`
  color: ${({ isLaunchSucceed }) => (isLaunchSucceed ? 'green' : 'red')};
`

const Links = styled.div`
  div {
    margin: 0;
  }
`
