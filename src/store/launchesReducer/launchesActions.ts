import { iLaunchData } from '../../components/LaunchCard'

export enum LaunchesActionTypes {
  SET_PAST_LAUNCHES,
  SET_LAUNCHES,
  SET_BOOKED_LAUNCHES,
  BOOK_LAUNCH,
  CANCEL_LAUNCH_BOOKING,
}

interface SetPastLaunchesAction {
  type: LaunchesActionTypes.SET_PAST_LAUNCHES
  payload: { data: iLaunchData[] }
}

interface SetLaunchesAction {
  type: LaunchesActionTypes.SET_LAUNCHES
  payload: { data: iLaunchData[] }
}

interface SetBookedLaunchesAction {
  type: LaunchesActionTypes.SET_BOOKED_LAUNCHES
  payload: { data: iLaunchData[] }
}

interface BookLaunchAction {
  type: LaunchesActionTypes.BOOK_LAUNCH
  payload: { id: string }
}

interface CancelLaunchBookingAction {
  type: LaunchesActionTypes.CANCEL_LAUNCH_BOOKING
  payload: { id: string }
}

export type LaunchesAction =
  | SetPastLaunchesAction
  | SetLaunchesAction
  | SetBookedLaunchesAction
  | BookLaunchAction
  | CancelLaunchBookingAction
