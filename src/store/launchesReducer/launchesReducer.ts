import { LaunchesAction, LaunchesActionTypes } from './launchesActions'
import { iLaunchData } from '../../components/LaunchCard'

interface iLaunchesState {
  pastLaunches: iLaunchData[]
  launches: iLaunchData[]
  bookedLaunches: iLaunchData[]
}

const launchMockData: iLaunchData = {
  id: '',
  name: '',
  date: new Date(),
  flightNumber: 0,
  success: true,
  loading: true,
  rocket: '',
  details: null,
  links: {
    presskit: null,
    article: null,
    wikipedia: null,
  },
}

const initialState: iLaunchesState = {
  pastLaunches: [...Array(4)].map(() => launchMockData),
  launches: [...Array(3)].map(() => launchMockData),
  bookedLaunches: [],
}

export const launchesReducer = (
  state = initialState,
  action: LaunchesAction
): iLaunchesState => {
  switch (action.type) {
    case LaunchesActionTypes.SET_PAST_LAUNCHES:
      return {
        ...state,
        pastLaunches: action.payload.data,
      }

    case LaunchesActionTypes.SET_LAUNCHES:
      return {
        ...state,
        launches: action.payload.data,
      }

    case LaunchesActionTypes.SET_BOOKED_LAUNCHES:
      return {
        ...state,
        bookedLaunches: action.payload.data,
      }

    case LaunchesActionTypes.BOOK_LAUNCH: {
      const { launches, bookedLaunches } = state
      const cancelingLaunchBooking = launches.find(
        (launch) => launch.id === action.payload.id
      )
      return {
        ...state,
        launches: launches.filter((launch) => launch.id !== action.payload.id),
        bookedLaunches: cancelingLaunchBooking
          ? [cancelingLaunchBooking, ...bookedLaunches]
          : bookedLaunches,
      }
    }

    case LaunchesActionTypes.CANCEL_LAUNCH_BOOKING: {
      const { launches, bookedLaunches } = state
      const cancelingLaunchBooking = bookedLaunches.find(
        (launch) => launch.id === action.payload.id
      )

      return {
        ...state,
        launches: cancelingLaunchBooking
          ? [...launches, cancelingLaunchBooking]
          : launches,
        bookedLaunches: state.bookedLaunches.filter(
          (launch) => launch.id !== action.payload.id
        ),
      }
    }

    default:
      return state
  }
}
